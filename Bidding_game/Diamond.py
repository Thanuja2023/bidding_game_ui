    
import pygame
import sys
import random
import os

# Initialize Pygame
pygame.init()

# Set up the window
WIDTH, HEIGHT = 800, 600
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Diamond Bidding Game")

# Define colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# Load diamond card images
diamond_images = []
unused_diamonds = list(range(2, 15))  # Initialize all card values
for i in range(2, 15):
    diamond_images.append(pygame.image.load(r"C:\Users\prama\Downloads\images\{}_of_diamonds.png".format(i)))
# Function to display text on the screen
def draw_text(text, font, color, x, y):
    text_surface = font.render(text, True, color)
    text_rect = text_surface.get_rect(center=(x, y))
    screen.blit(text_surface, text_rect)

# Function to display player scores
def display_scores(player_scores, font):
    for i, (player, score) in enumerate(player_scores.items()):
        draw_text(f"{player}: {score}", font, BLACK, WIDTH // 2, 50 + i * 40)

# Function to display all diamond card images
def display_all_diamond_cards(images):
    num_cards = len(images)
    card_width = 80
    card_height = 120
    horizontal_spacing = 20
    total_width = num_cards * (card_width + horizontal_spacing) - horizontal_spacing
    start_x = (WIDTH - total_width) // 2
    y = HEIGHT // 2 - card_height // 2

    for i, image in enumerate(images):
        x = start_x + i * (card_width + horizontal_spacing)
        screen.blit(pygame.transform.scale(image, (card_width, card_height)), (x, y))

# Function to display the selected diamond card
def display_selected_card(image):
    selected_card_width = 100
    selected_card_height = 150
    x = (WIDTH - selected_card_width) // 2
    y = 50
    screen.blit(pygame.transform.scale(image, (selected_card_width, selected_card_height)), (x, y))

# Main game function
def main():
    # Player setup
    players = ["Player 1", "Player 2"]
    player_scores = {player: 0 for player in players}

    # Load font
    font = pygame.font.SysFont(None, 40)

    # Main game loop
    running = True
    while running:
        # Handle events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        # Clear the screen
        screen.fill(WHITE)

        # Randomly select a diamond card for bidding
        diamond_card_value = random.choice(unused_diamonds)
        selected_diamond_image = diamond_images[diamond_card_value - 2]
        unused_diamonds.remove(diamond_card_value)

        # Display all diamond cards
        display_all_diamond_cards(diamond_images)

        # Display the selected card separately
        display_selected_card(selected_diamond_image)

        # Display scores
        display_scores(player_scores, font)

        # Update the display
        pygame.display.flip()

        # Prompt players for bidding
        bids = {}
        for player in players:
            bid = None
            while bid is None:
                try:
                    bid = int(input(f"{player}, enter your bid (2-14): "))
                    if bid < 2 or bid > 14:
                        print("Invalid bid! Bid must be between 2 and 14.")
                        bid = None
                    elif bid in bids.values():
                        print("This bid has already been placed! Choose a different bid.")
                        bid = None
                except ValueError:
                    print("Invalid input! Please enter a number.")
                    bid = None
            bids[player] = bid

        # Determine winner(s) of the bid
        highest_bid = max(bids.values())
        winning_bidders = [player for player, bid in bids.items() if bid == highest_bid]

        # Calculate points and update scores
        points = diamond_card_value
        if len(winning_bidders) == 1:
            winner = winning_bidders[0]
            player_scores[winner] += points
        else:
            points_per_winner = points // len(winning_bidders)
            for winner in winning_bidders:
                player_scores[winner] += points_per_winner

        # If all cards have been used, end the game
        if not unused_diamonds:
            running = False

    # Quit Pygame
    pygame.quit()
    sys.exit()

if __name__ == "__main__":
    main()
